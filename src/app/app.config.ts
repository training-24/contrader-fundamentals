import { HTTP_INTERCEPTORS, HttpErrorResponse, provideHttpClient, withInterceptors } from '@angular/common/http';
import { ApplicationConfig } from '@angular/core';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { catchError, of, throwError } from 'rxjs';

import { routes } from './app.routes';
import { authInterceptor } from './core/auth/auth.interceptor';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes, withComponentInputBinding()),
    provideHttpClient(
      withInterceptors([
        authInterceptor,
        (req: any, next: any) => {
          // console.log('inteceptor 2')
          return next(req)
            .pipe(
              catchError(err => {
                // console.log('interceptor 2 error')
                return throwError(err)
              })
            )
        }]
      )
    ),
  ],
};
