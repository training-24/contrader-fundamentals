import { Component} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavbarComponent } from './core/components/navbar.component';
import { TodosService } from './features/demo-signals/todo-list/services/todos.service';

@Component({
  selector: 'app-root',
  standalone: true,
  template: `
    <app-navbar />
    <hr>
    <router-outlet />
  `,
  imports: [
    RouterOutlet,
    NavbarComponent
  ],
  /*providers: [
    TodosService
  ]*/
})
export class AppComponent {

}
