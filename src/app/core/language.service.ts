import { Injectable, signal } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  lang = 'it'
  language = signal<'it' | 'en'>('it')

  setLanguage(lang: 'it' | 'en' ) {
    this.lang = lang;
    this.language.set(lang)
  }
}
