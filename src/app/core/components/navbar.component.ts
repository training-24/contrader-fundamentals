import { ChangeDetectionStrategy, Component, effect, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { HttpErrorMsgComponent } from '../../shared/http-error-msg.component';
import { LanguageService } from '../language.service';

@Component({
  selector: 'app-navbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    RouterLink,
    FormsModule,
    HttpErrorMsgComponent
  ],
  template: `
    
    <div class="join ">
      <button routerLink="signals-demo" class="btn btn-secondary btn-xs join-item">Demo Signal</button>
      <button routerLink="http-demo" class="btn btn-secondary btn-xs join-item">Demo Http</button>
    </div>
    
    {{langSrv.language()}} - {{langSrv.lang}} -
    
  `,
  styles: ``
})
export class NavbarComponent {
  langSrv = inject(LanguageService)

  constructor() {
    effect(() => {
      console.log('ciao')
    });
  }
}
