import { HttpErrorResponse, HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, of, throwError } from 'rxjs';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const router = inject(Router)
  const tk = localStorage.getItem('token')

  let cloneReq = req;
  if (tk) {
    cloneReq = cloneReq.clone({
      setHeaders: {
        Authorization: 'Bearer ' + tk
      },
    })

  }
  // console.log('interceptor 1')

  return next(cloneReq)
    .pipe(
      catchError(err => {
        if (err instanceof HttpErrorResponse) {
          switch(err.status) {
            // case 0:
            case 401:
              router.navigateByUrl('login')
              break;
          }
        }
       //  console.log('interceptor 1 error')
        return throwError(err)
      })
    )
};
