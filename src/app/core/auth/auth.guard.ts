import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

export const authGuard: CanActivateFn = () => {
  return  true;
  // check token in localstorage
  // console.log('guardia')
  const router = inject(Router)
  const token = localStorage.getItem('token')
  if (!token) {
    router.navigateByUrl('login')
  }
  return !!token
};
