import { Routes } from '@angular/router';
import { authGuard } from './core/auth/auth.guard';
import { demoHttpRoutes } from './features/demo-http/routes';

export const routes: Routes = [
  {
    path: 'login',
    loadComponent: () => import('./features/login.component')
  },
  {
    path: 'signals-demo',
    loadComponent: () => import('./features/demo-signals/demo-signals.component'),
    children: [
      {
        path: 'demo1',
        loadComponent: () => import('./features/demo-signals/components/demo1-signal.component'),
      },
      {
        path: 'demo1B',
        loadComponent: () => import('./features/demo-signals/components/demo1-signal.component'),
      },
      { path: 'demo2', loadComponent: () => import('./features/demo-signals/components/demo2-signal.component')},
      { path: 'demo3', loadComponent: () => import('./features/demo-signals/components/demo3-signal.component')},
      { path: 'demo4', loadComponent: () => import('./features/demo-signals/components/demo4-for.component')},
      { path: 'demo5', loadComponent: () => import('./features/demo-signals/todo-list/demo5-todolist.component')},
      { path: 'demo6', loadComponent: () => import('./features/demo-signals/components/demo6.component')},
      { path: '', redirectTo: 'demo1', pathMatch: 'full'}
    ]
  },
  {
    path: 'http-demo',
    loadComponent: () => import('./features/demo-http/demo-http.component'),
    children: demoHttpRoutes,
    canActivate: [authGuard, () => {
      // console.log('here seconda guardia')
    }]
  },
  { path: '', redirectTo: 'demo1', pathMatch: 'full'},

];


