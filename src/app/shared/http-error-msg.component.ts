import { Component, inject, Input } from '@angular/core';
import { TodosService } from '../features/demo-signals/todo-list/services/todos.service';

@Component({
  selector: 'app-http-error-msg',
  standalone: true,
  imports: [],
  template: `
    @if (todosrv.error() !== null) {
      <div class="alert alert-error my-3">
        ERROR - status: {{todosrv.error()}}
        @switch (todosrv.error()) {
          @case (0) {
            <div>SERVER DOWN</div>
          }
          @case (402) {
            <div>ANOTHER PROBLEM</div>
          }
        }
      </div>
    }
  `,
})
export class HttpErrorMsgComponent {
  // @Input() error: number | null = null;
  todosrv = inject(TodosService)
}
