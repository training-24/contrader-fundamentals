import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [],
  template: `
    <p>
      card works!
    </p>

    {{render()}}
  `,
  styles: ``
})
export class CardComponent {

  render() {
    console.log('render card')
  }
}
