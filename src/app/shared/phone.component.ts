import { NgIf } from '@angular/common';
import { booleanAttribute, ChangeDetectionStrategy, Component, Input, numberAttribute } from '@angular/core';

export type Size = 'sm' | 'md' | 'lg'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-phone',
  standalone: true,
  imports: [
    NgIf
  ],
  template: `
    <div class="mockup-phone">
      <div class="camera"></div>
      <div class="display">
        <div class="artboard artboard-demo phone-1">
          @if(showTitle) {
            <h1>{{title}}</h1>
          }
          
          @if(url) {
            <img
              [src]="url" 
              alt=""
              [style.width.%]="size"
            >
          }
        </div>
      </div>
    </div>
  `,
  styles: ``
})
export class PhoneComponent {
  @Input({ transform: (val: string) => {
    return val.toUpperCase()
  }})
  title: string = ''

  @Input({ required: true, alias: 'src' })
  url = ''

  @Input({ transform: booleanAttribute})
  showTitle = false

  @Input({ transform: (val: Exclude<Size, 'xl'>) => {

      switch(val) {
        case 'sm': return 50;
        case 'md': return 75;
        default:
        case 'lg': return 100;
      }
  } })
  size: number = 100

  render() {
    console.log('render phone')
  }

}
