import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { TodosService } from '../services/todos.service';

// ZONELESS

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-todo-summary',
  standalone: true,
  imports: [],
  template: `
    <div>{{todosCompleted }} completed - {{ todosToComplete}} todos</div>
  `,
})
export class TodoSummaryComponent {
  @Input() todosCompleted: number = 0
  @Input() todosToComplete: number = 0
}
