import { NgClass } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Todo } from '../../../../model/todo';
import { TodoListItemsComponent } from './todo-list-items.component';

@Component({
  selector: 'app-todo-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    NgClass,
    TodoListItemsComponent
  ],
  template: `
    @for (todo of todos; track todo.id) {
      <app-todo-list-items 
        [todo]="todo"
        (toggleTodo)="toggleTodo.emit($event)"
        (removeTodo)="removeTodo.emit($event)"
      />
    }
  `,
  styles: ``
})
export class TodoListComponent {
  @Input() todos: Todo[] = [];
  @Output() toggleTodo = new EventEmitter<Todo>()
  @Output() removeTodo = new EventEmitter<Todo>()

}
