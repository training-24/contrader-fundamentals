import { NgClass } from '@angular/common';
import { Component, EventEmitter, Input, Output, signal } from '@angular/core';
import { Todo } from '../../../../model/todo';

@Component({
  selector: 'app-todo-list-items',
  standalone: true,
  imports: [
    NgClass
  ],
  template: `
    <div class="flex gap-3 justify-between items-center border-b border-slate-600 py-3">
      <div class="flex gap-2">
        <input
          type="checkbox"
          [checked]="todo.completed"
          (change)="toggleTodo.emit(todo)"
        >
        <div
          class="text-xl"
          [ngClass]="{'line-through': todo.completed }"
        >
          {{todo.title}}
          @if(isOpen()) {
            <div>{{todo.description}}</div>
          }
        </div>
      </div>

      <div class="flex gap-3">
        <button class="my-button" (click)="showDetails(todo.id)">dettagli</button>
        <button class="my-button"
                (click)="removeTodo.emit(todo)">delete</button>
      </div>
    </div>
  `,
  styles: ``
})
export class TodoListItemsComponent {
  @Input({ required: true })  todo!: Todo
  @Output() toggleTodo = new EventEmitter<Todo>()
  @Output() removeTodo = new EventEmitter<Todo>()
  isOpen = signal(false)

  showDetails(id: number) {
    this.isOpen.update(prev => !prev)
  }
}

// non null assertion operator
