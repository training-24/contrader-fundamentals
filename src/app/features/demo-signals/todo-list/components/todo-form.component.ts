import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-todo-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [],
  template: `
    <input
      type="text" placeholder="add todo"
      (keydown.enter)="addTodo.emit(inputRef)"
      #inputRef
    >
    
    <!--{{render()}}-->
  `,
  styles: ``
})
export class TodoFormComponent {
  @Output() addTodo = new EventEmitter<HTMLInputElement>();

  render() {
    console.log('todo form: render')
  }
}
