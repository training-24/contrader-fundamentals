import { JsonPipe, NgClass } from '@angular/common';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, computed, inject, signal, ViewEncapsulation } from '@angular/core';
import { Todo } from '../../../model/todo';
import { HttpErrorMsgComponent } from '../../../shared/http-error-msg.component';
import { TodoFormComponent } from './components/todo-form.component';
import { TodoListComponent } from './components/todo-list.component';
import { TodoSummaryComponent } from './components/todo-summary.component';
import { TodosService } from './services/todos.service';

@Component({
  selector: 'app-demo5-todolist',
  // encapsulation: ViewEncapsulation.ShadowDom,
  standalone: true,
  imports: [
    NgClass,
    JsonPipe,
    HttpErrorMsgComponent,
    TodoSummaryComponent,
    TodoFormComponent,
    TodoListComponent
  ],
  template: `
    <h1>Todo List</h1>

    <app-http-error-msg/>
    <app-todo-form 
      (addTodo)="todosServices.addTodo($event)" 
    />
    
    <app-todo-summary 
      [todosCompleted]="todosServices.todosCompleted()"   
      [todosToComplete]="todosServices.todosToComplete()"
    />
    <app-todo-list
      (toggleTodo)="todosServices.toggleTodo($event)"
      (removeTodo)="todosServices.removeTodo($event)"
      [todos]="todosServices.todos()"
    />
    
  `,
  providers: [
    // TodosService,
    { provide: TodosService, useClass: TodosService },
    // { provide: MapService, useClass: OpenStreetMapService }
    //{ provide: LogService, useClass: AnotherService}
  ]
})
export default class Demo5TodolistComponent {
  todosServices = inject(TodosService)
  // logServce = inject(LogService)

  constructor() {
    this.todosServices.loadTodos()
  }

  ngOnDestroy() {
    console.log('destreoy compo')
  }

}

