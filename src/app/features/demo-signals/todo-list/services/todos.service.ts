import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { computed, inject, Injectable, signal } from '@angular/core';
import { Todo } from '../../../../model/todo';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  http = inject(HttpClient)
  todos = signal<Todo[]>([])
  error = signal<number | null>(null)
  todosCompleted = computed(() => this.todos().filter(t => t.completed).length)
  todosToComplete = computed(() => this.todos().filter(t => !t.completed).length)

  isOpen: boolean = false;

  constructor() {
    console.log('init to service')
  }


  loadTodos() {
    this.http.get<Todo[]>('http://localhost:3000/todos')
      .subscribe({
        next: res => {
          this.todos.set(res);
        },
        error: (err: any) => {
          if (err instanceof HttpErrorResponse) {
            this.error.set(err.status)
          }
        }
      })
  }


  addTodo(input: HTMLInputElement) {
    this.error.set(null)
    //const newTodo: Omit<Todo, 'id' | 'description'> = {
    const newTodo: Pick<Todo, 'title' | 'completed'> = {
      title: input.value,
      completed: false
    }

    this.http.post<Todo>('http://localhost:3000/todos', newTodo)
      .subscribe({
        next: res => {
          this.todos.update(todos => [...todos, res])
        },
        error: (err: any) => {
          if (err instanceof HttpErrorResponse) {
            this.error.set(err.status)
          }
        }
      })

    input.value = ''
  }

  removeTodo(todoToRemove: Todo) {
    this.error.set(null)
    this.http.delete(`http://localhost:3000/todos/${todoToRemove.id}`)
      .subscribe(() => {
        this.todos.update(
          todos => todos.filter(t => t.id !== todoToRemove.id)
        )
      })


  }

  toggleTodo(todoToToggle: Todo) {
    this.error.set(null)
    this.http.patch<Todo>(`http://localhost:3000/todos/${todoToToggle.id}`, {
      completed: !todoToToggle.completed
    })
      .subscribe((todoUpdated) => {
        this.todos.update(todos => todos.map(
          t => t.id === todoToToggle.id ? todoUpdated : t)
        )
      })

  }

  ngOnDestroy() {
    console.log('destreoy srv')

  }
}
