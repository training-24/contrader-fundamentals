import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-demo-signals',
  standalone: true,
  imports: [
    RouterOutlet,
    RouterLink
  ],
  template: `
    <button class="btn btn-info btn-xs join-item" routerLink="demo1">demo1</button>
    <button class="btn btn-info btn-xs join-item" routerLink="demo2">demo2</button>
    <button class="btn btn-info btn-xs join-item" routerLink="demo3">demo3</button>
    <button class="btn btn-info btn-xs join-item" routerLink="demo4">demo4</button>
    <button class="btn btn-info btn-xs join-item" routerLink="demo5">demo5</button>
    <button class="btn btn-info btn-xs join-item" routerLink="demo6">demo6</button>

    <hr>
    <router-outlet />
  `,
  styles: ``
})
export default class DemoSignalsComponent {

}
