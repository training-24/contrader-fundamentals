import { Component, signal } from '@angular/core';

@Component({
  selector: 'app-demo3-signal',
  standalone: true,
  imports: [],
  template: `
    <h1>Switch</h1>

    @switch (currentStep()) {
      @case ('one') {
        <h1>STEP 1 </h1>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam atque autem beatae distinctio eaque eligendi enim harum illum maxime natus neque, officia pariatur quia quo recusandae sequi suscipit tenetur totam.
        <button (click)="currentStep.set('two')">go to step 2</button>
        
      }
      @case ('two') {
        <h1>STEP 2 </h1>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam atque autem beatae distinctio eaque eligendi enim harum illum maxime natus neque, officia pariatur quia quo recusandae sequi suscipit tenetur totam.
      }
      @default {
        <h1>HELLO!! </h1>
        ...
        <button (click)="currentStep.set('one')">go to step 1</button>
      }
      
    }
    
  `,
  styles: ``
})
export default class Demo3SignalComponent {
  currentStep = signal<'one' | 'two' | null>(null)
}
