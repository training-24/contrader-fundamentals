import { JsonPipe, NgForOf } from '@angular/common';
import { Component, computed, inject, signal } from '@angular/core';
import { TodosService } from '../todo-list/services/todos.service';


export type Product = {
  id: number;
  name: string;
  cost: number;
}

const initialState: Product[] = [
  {id: 1, name: 'Chocolate', cost: 3},
  {id: 2, name: 'Milk', cost: 1},
  {id: 3, name: 'Biscuits', cost: 2},
]

@Component({
  selector: 'app-demo4-for',
  standalone: true,
  imports: [
    JsonPipe,
    NgForOf
  ],
  template: `
    <h1>for block</h1>
   

    @if(noItems()) {
      <div>non ci sono elementi </div>
    }

    <hr>
    @for(product of products(); track product.id) {
      <li>
        {{$index + 1}} {{ product.name }} - {{$first}} {{$even}} {{$count}}
      </li>
    } @empty {
      <button (click)="loadProducts()">load</button>
    }
  
    
    <!--
    @for(product of products(); track product.id + product.name) {
      
    }
    
    @for(product of products(); track identify(product)) {
      <li>{{ product.id }}</li>
    }-->
    
    
   <!-- <pre>{{products() | json}}</pre>-->
  `,
  styles: ``
})
export default class Demo4ForComponent {
  products = signal<Product[]>([])
  noItems = computed(() => this.products().length === 0)

  loadProducts() {
    this.products.set(initialState)
  }




  identify(item: Product) {
    console.log(item)
    return item.id
  }
}
