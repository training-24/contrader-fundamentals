import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PhoneComponent } from '../../../shared/phone.component';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [
    PhoneComponent,
    CommonModule,
    FormsModule
  ],
  template: `
    <h1>Demo Components</h1>
    
    <h1>{{value}}</h1>
    <input type="text" [(ngModel)]="value">
    
    <div>
    <app-phone
      [title]="value"
      src="https://www.format.com/wp-content/uploads/symmetrical_clouds_reflection_in_water-724x1024.jpg"
      showTitle
      size="sm"
    />
    
    <app-phone
      title="Widget phone"
      src="https://www.format.com/wp-content/uploads/symmetrical_clouds_reflection_in_water-724x1024.jpg"
    />
    </div>
  `,
  styles: ``
})
export default class Demo6Component {
  value = 'PIppo'

  render() {
    console.log('render parent')
  }
}
