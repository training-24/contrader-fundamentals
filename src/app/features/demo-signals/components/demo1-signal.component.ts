import { NgIf } from '@angular/common';
import { Component, computed, effect, signal } from '@angular/core';

@Component({
  selector: 'app-demo1-signal',
  standalone: true,
  imports: [
    NgIf
  ],
  template: `
    <h1>Demo1 Signal</h1>

    <div *ngIf="isZero()">il counter è zero</div>

    <h1
      [style.color]="isZeroColor()"
    >Counter {{counter()}}</h1>

    <button (click)="dec()">-</button>
    <button (click)="inc()">+</button>

    <button
      (click)="reset()"
      [style.display]="hideIfZero()"
    >reset</button>

    <hr>
    <div>
      <button (click)="doNOthing()">do nothing</button>
    </div>
  `,
  styles: ``
})
export default class Demo1SignalComponent {
  counter = signal(0)
  isZero = computed(() => this.counter() === 0 )
  isZeroColor = computed(() => this.isZero() ? 'red' : 'green')
  hideIfZero = computed(() => this.isZero() ? 'none' : 'inline')

  constructor() {
    effect(() => {
      localStorage.setItem('counter', this.counter().toString())
    }, { allowSignalWrites: true });
  }

  inc() {
    this.counter.update(c => c + 1)
  }

  dec() {
    this.counter.update(c => c - 1)
  }

  reset() {
    this.counter.set(0)
  }

  doNOthing() {
    console.log('doNOthing')
  }
}
