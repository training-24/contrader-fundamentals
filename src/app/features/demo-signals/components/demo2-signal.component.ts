import { NgIf } from '@angular/common';
import { Component, signal } from '@angular/core';

@Component({
  selector: 'app-demo2-signal',
  standalone: true,
  imports: [
    NgIf
  ],
  template: `
    <h1>Demo if!</h1>
    
    <button (click)="toggle()">toggle</button>

    <h1>ngIf</h1>
    <ng-container *ngIf="visible(); else tpl">Hello ngIF</ng-container>
    
    <ng-template #tpl>
      NOTHING!!!
    </ng-template>

    <hr>
    <h1>if block</h1>
    
    @if(visible()) {
      Hello if block
    } @else {
      Nothing!!!
    }
  `,
  styles: ``
})
export default class Demo2SignalComponent {
  visible = signal(false)

  toggle() {
    this.visible.update(prev => !prev)
  }
}
