import { Routes } from '@angular/router';

export const demoHttpRoutes: Routes =  [
  {
    path: 'demo7',
    loadComponent: () => import('./components/demo7.component'),
    data: { title: 'Hello Demo 7'}
  },
  { path: 'demo8/:id', loadComponent: () => import('./components/demo8.component')},
  { path: 'demo9', loadComponent: () => import('./components/demo9.component')},
  { path: '', redirectTo: 'demo7', pathMatch: 'full'}
]
