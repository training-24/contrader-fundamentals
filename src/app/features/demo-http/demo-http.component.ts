import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-demo-http',
  standalone: true,
  imports: [
    RouterOutlet,
    RouterLink
  ],
  template: `
    <button class="btn btn-info btn-xs join-item" routerLink="demo7">demo7</button>
    <button class="btn btn-info btn-xs join-item" routerLink="demo8/1">demo8</button>
    <button class="btn btn-info btn-xs join-item" routerLink="demo9">demo9</button>
    
    <router-outlet />
    
  `,
  styles: ``
})
export default class DemoHttpComponent {

}
