import { log } from '@angular-devkit/build-angular/src/builders/ssr-dev-server';
import { AsyncPipe, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, Input } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { catchError, map, mergeAll, mergeMap, of } from 'rxjs';
import { User } from '../../../model/user';

@Component({
  selector: 'app-demo8',
  standalone: true,
  imports: [
    RouterLink,
    JsonPipe,
    AsyncPipe
  ],
  template: `
    <p>
      ID: 
    </p>
    
    <div class="flex gap-2">
      <button routerLink="../1">1</button>
      <button routerLink="../2">2</button>
      <button routerLink="../3">3</button>
    </div>
    
    <pre>{{user | json}}</pre>
    <pre>{{user$ | async | json}}</pre>
  `,
  styles: ``
})
export default class Demo8Component {
  // @Input() id: string | undefined;
  @Input() set id(val: string) {
    this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + val)
      .subscribe(res => {
        this.user = res
      })
  }
  user: User | undefined

  activateRoute = inject(ActivatedRoute)
  router = inject(Router)
  http = inject(HttpClient)

  user$ = this.activateRoute.params
    .pipe(
      mergeMap(
        (params) =>  this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + params['id'])
          .pipe(
            catchError(err => of(null) )
          )
      ),
    )

}
