import { Component, inject } from '@angular/core';
import { LanguageService } from '../../../core/language.service';

@Component({
  selector: 'app-demo9',
  standalone: true,
  imports: [],
  template: `
    <h1>Demo Language Service</h1>
    
    <button (click)="langSrv.setLanguage('it')">it</button>
    <button (click)="langSrv.setLanguage('en')">En</button>
  `,
  styles: ``
})
export default class Demo9Component {
  langSrv = inject(LanguageService)

}
