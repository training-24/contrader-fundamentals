import { log } from '@angular-devkit/build-angular/src/builders/ssr-dev-server';
import { AsyncPipe } from '@angular/common';
import { Component, inject, Input, OnDestroy } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ActivatedRoute } from '@angular/router';
import { interval, map, Subject, Subscription, takeUntil } from 'rxjs';

@Component({
  selector: 'app-demo7',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  template: `
    <p>
      demo7 works! {{title$ | async}}
      {{title}}
    </p>
  `,
})
export default class Demo7Component {
  @Input() title: string  = ''

  activateRoute = inject(ActivatedRoute);
  title$ = this.activateRoute.data
    .pipe(
      map(data => data['title']),
    )

  constructor() {
   /* this.activateRoute.data
      .subscribe({
        next: res => console.log(res),
        error: err => console.log(err)
      })*/
  }
}
